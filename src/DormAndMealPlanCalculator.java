/**
 * Created by Dmitry on 7/20/2015.
 */
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;

public class DormAndMealPlanCalculator extends JFrame {
    private final int WINDOW_WIDTH = 360;
    private final int WINDOW_HEIGHT = 200;

    private JPanel dormPanel;
    private JPanel mealPanel;
    private JPanel buttonPanel;

    private JLabel dormLabel;
    private JLabel mealLabel;

    private JComboBox dormBox;
    private JComboBox mealBox;

    private JButton calcButton;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem exitItem;

    private int charges;

    private String[] dorms = {"Allen Hall", "Pike Hall", "Farthing Hall", "University Suites"};
    private int[] dormCharges = {1500, 1600, 1200, 1800};

    private String[] meals = {"7 meals per week", "14 meals per week", "Unlimited meals"};
    private int [] mealCharges = {560, 1095, 1500};


    public DormAndMealPlanCalculator(){
        this.setTitle("Dorm and Meal Plan Calculator");
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());

        buildDormPanel();
        buildMealPanel();
        buildButtonPanel();

        this.add(dormPanel);
        this.add(mealPanel);
        this.add(buttonPanel);

        buildMenuBar();
        this.setJMenuBar(menuBar);

        this.setVisible(true);
        setLocationRelativeTo(null);
    }

    private void buildDormPanel(){
        dormPanel = new JPanel();
        dormLabel = new JLabel("Select a Dorm.");
        dormBox = new JComboBox(dorms);

        dormPanel.add(dormLabel);
        dormPanel.add(dormBox);
    }

    private void buildMealPanel(){
        mealPanel = new JPanel();
        mealLabel = new JLabel("Select a Meal Plan.");
        mealBox = new JComboBox(meals);

        mealPanel.add(mealLabel);
        mealPanel.add(mealBox);
    }

    private void buildButtonPanel(){
        buttonPanel = new JPanel();
        calcButton = new JButton("Calculate Charges");
        calcButton.addActionListener(new CalcButtonListener());

        buttonPanel.add(calcButton);
    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();
        buildFileMenu();
        menuBar.add(fileMenu);
    }

    private void buildFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.addActionListener(new ExitListener());

        fileMenu.add(exitItem);
    }

    private class CalcButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int selectedDorm = dormBox.getSelectedIndex();
            int selectedMeal = mealBox.getSelectedIndex();

            charges = dormCharges[selectedDorm] + mealCharges[selectedMeal];

            DecimalFormat dollar = new DecimalFormat("#,##0.00");
            JOptionPane.showMessageDialog(null, "Total Charges per Semester: $" + dollar.format(charges));
        }
    }

    private class ExitListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }

    public static void main(String[] args){
        new DormAndMealPlanCalculator();
    }
}

